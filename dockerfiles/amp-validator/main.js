'use strict';

const scrape = require('website-scraper');
var fs = require('fs');
var amphtmlValidator = require('amphtml-validator');

async function downloadHtmlPage(url, filename) {
  const options = {
    urls: [{ url: url, filename: filename }],
    directory: download_directory,
    sources: []
  };
  await scrape(options);
}
async function checkPage(filename) {
  var input = fs.readFileSync(download_directory + '/' + filename, 'utf8');
  var validator = await amphtmlValidator.getInstance();
  var result = validator.validateString(input);
  return result;
}
async function ampCheck(url) {
  var filename = url.split('/')[2].replace('/', '_')
  filename == '' ? filename = "index.html" : filename = filename + ".html"

  await downloadHtmlPage(url, filename);
  var result = await checkPage(filename);
  result["url"] = url;
  return result;
}
async function checkAll(urls_file, results_file, testsuite_name, testclass_name, report_file) {
  var urls = JSON.parse(fs.readFileSync(urls_file, 'utf8'));

  var itemsProcessed = 0;
  var results = [];
  urls.forEach( (element) => {
    ampCheck(element).then(value => {
      results.push(value);
      itemsProcessed++;
      if(itemsProcessed === urls.length) {
        fs.writeFileSync(results_file, JSON.stringify(results), 'utf8');
        junitReport(results_file, testsuite_name, testclass_name, report_file);
      }
    });
  });
}
function junitReport(results_file, testsuite_name, testclass_name, filename){
  var results = JSON.parse(fs.readFileSync(results_file, 'utf8'));
  var builder = require('junit-report-builder');
  var suite = builder.testSuite().name(testsuite_name);

  results.forEach( result => {
    if(result.status == 'PASS') {
      var testCase = suite.testCase()
        .className(testclass_name)
        .name(result.url);
    } else {
      var testCase = suite.testCase()
        .className(testclass_name)
        .name(result.url)
        .failure();
    }
  })
  builder.writeTo(filename);
}

const download_directory = "./pages";
const urls_file = "./resources/input.json";
const results_file = "./resources/results.json";
const report_file = "./resources/results.xml";
const testsuite_name = "b2p-prd";

if (fs.existsSync(download_directory)) { fs.rmdirSync(download_directory, { recursive: true }) }
checkAll(urls_file, results_file, testsuite_name, "AMP", report_file);