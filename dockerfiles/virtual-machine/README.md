## virtual-machine

**Docker Registry URL**: https://gitlab.com/nevertheless.space/docker-registry/container_registry

### Description

Ubuntu + SSH on port 22: root password `root`.

- OS: **ubuntu:20.04**
- Installations:
  - **openssh-server**
- Entrypoint: `/usr/sbin/sshd -D`