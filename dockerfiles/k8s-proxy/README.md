## k8s-proxy

**Docker Registry URL**: https://gitlab.com/nevertheless.space/docker-registry/container_registry

### Description

Kubernetes cluster connection + port forward.

- OS: **ubuntu:20.04**
- Installations:
  - **kubectl** 1.18.5-00
- Entrypoint: start.sh
- Environment Variables:
  - **KUBE_CONFIG**: base64 kubeconfig file
  - **PORTFORWARD_ARGS**: port forwarding arguments, for instance: `"service/prometheus 4001:80 -n monitoring"` as `"<resource type>/<resource name> <host port>:<container port> -n <namespace>"`