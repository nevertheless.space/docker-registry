## heroku-cli

**Docker Registry URL**: https://gitlab.com/nevertheless.space/docker-registry/container_registry

### Description

- OS: **node:10-alpine**
- Installations:
  - **curl** 
  - **heroku-cli** 7.47.12
  - **docker**