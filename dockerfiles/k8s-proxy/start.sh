# !/bin/bash

kubectl version

echo "${KUBE_CONFIG}" | base64 --decode > kube.config
export KUBECONFIG=$(pwd)/kube.config

kubectl config view
kubectl config get-clusters

kubectl port-forward --address 0.0.0.0 $PORTFORWARD_ARGS 