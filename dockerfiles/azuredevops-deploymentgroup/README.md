## azuredevops-deploymentgroup

**Docker Registry URL**: https://gitlab.com/nevertheless.space/docker-registry/container_registry

### Description

- OS: **ubuntu:18.04**
- Installations:
  - **ca-certificates**
  - **curl**
  - **jq**
  - **git**
  - **iputils-ping**
  - **libcurl4**
  - **libicu60**
  - **libunwind8**
  - **netcat**
  - **libssl1.0**
- Entrypoint: start.sh
- Environment Variables:
  - **AZP_URL**: Azure DevOps - URL
  - **AZP_TOKEN**: Azure DevOps - Personal Access Token (PAT)
  - **AZP_PRJ_NAME**: Azure DevOps - Project Name
  - **AZP_COLL_NAME**: Azure DevOps - Collection Name
  - **AZP_DG_NAME**: Azure DevOps - Deployment Group Name
  - **AZP_DG_TAGS**: Azure DevOps - Deployment Group Tags
    - Example: `"tag1, tag2, tag3"`
  - **AZP_AGENT_NAME**: Azure DevOps - Agent Registration Name
  - **AZP_WORK**: Azure DevOps - Agent Working Directory