## linux-shell

**Docker Registry URL**: https://gitlab.com/nevertheless.space/docker-registry/container_registry

### Description

- OS: **ubuntu:18.04**
- Installations:
  - **telnet**
  - **iputils-ping**
  - **screen**
  - **curl** 
  - **docker.io**
  - **dnsutils**
- Entrypoint: start.sh
  Permanent loop in order to get the pod **always** running:
  ```bash
  # !/bin/bash
  while true;
  do
    sleep 3600
  done
  ```