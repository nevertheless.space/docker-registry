# NeverTheLess - Docker Registry

## Images

**Docker Registry URL**: [Here to see all the images](https://gitlab.com/nevertheless.space/docker-registry/container_registry).

  - [amp-validator](/dockerfiles/amp-validator/README.md)
  - [azuredevops-agent](/dockerfiles/azuredevops-agent/README.md)
  - [azuredevops-deploymentgroup](/dockerfiles/azuredevops-deploymentgroup/README.md)
  - [heroku-cli](/dockerfiles/heroku-cli/README.md)
  - [k8s-proxy](/dockerfiles/k8s-proxy/README.md)
  - [kubectl-helm](/dockerfiles/kubectl-helm/README.md)
  - [linux-shell](/dockerfiles/linux-shell/README.md)
  - [linux-shell-headless](/dockerfiles/linux-shell-headless/README.md)
  - [terraform](/dockerfiles/terraform/README.md)
  - [terraform-k8s](/dockerfiles/terraform-k8s/README.md)
  - [virtual-machine](/dockerfiles/virtual-machine/README.md)