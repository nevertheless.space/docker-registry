## linux-shell-headless

**Docker Registry URL**: https://gitlab.com/nevertheless.space/docker-registry/container_registry

### Description

- OS: **ubuntu:18.04**
- Installations:
    - **telnet**
    - **iputils-ping**
    - **screen**
    - **curl** 
    - **docker.io**
    - **dnsutils**
    - **zip**