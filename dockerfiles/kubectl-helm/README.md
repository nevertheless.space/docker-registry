## kubectl-helm

**Docker Registry URL**: https://gitlab.com/nevertheless.space/docker-registry/container_registry

### Description
- OS: **alpine:3.11**
- Installations:
  - **kubectl** v1.18.2
    Note: Latest version of kubectl may be found at: https://github.com/kubernetes/kubernetes/releases
  - **helm** v3.2.0
    Note: Latest version of helm may be found at: https://github.com/kubernetes/helm/releases
  - [helm-push plugin](https://github.com/chartmuseum/helm-push/#readme)