## AMP Validator

**Docker Registry URL**: https://gitlab.com/nevertheless.space/docker-registry/container_registry

### Inputs

Array of http/https urls in `resorces/input.json` in json format.

### Outputs

Files:
- `resorces/results.json`: detailed results
- `resorces/results.xml`: junit report for CI tools